Given('o primeiro acesso ao Jogo',() => {
    cy.visit('https://sudoku-raravi.now.sh/')
})

And('que a dificuldade seja {string}',(dificuldade) => {
    cy.get('select[name="status__difficulty-select"]')
    .should('contain',dificuldade)
})

When('o timer começar a correr', () => {
    cy.xpath('/html/body/div/div[1]/div/section[2]/div[2]')
    .should('contain', '00:00')
    //cy.screenshot()
})

Then('é possível começar a Jogar',() => {
    //cy.screenshot()
})