Given('um Jogo em andamento',() => {
    cy.visit('https://sudoku-raravi.now.sh/')
})

And('que a dificuldade seja {string}',(dificuldade) => {
    cy.get('select[name="status__difficulty-select"]')
    .should('contain',dificuldade)
})

And('ao selecionar uma célula',() => {
    cy.wait(500)
    cy.get('tr.game__row:nth-child(1) > td:nth-child(1)')
    .click()
})

When('pedir uma Dica',() => {
    cy.get('div[class="status__action-hint"]')
    .click()
})

Then('o campo é preenchido',() => {
    cy.wait(500)
    cy.get('tr.game__row:nth-child(1) > td:nth-child(1)')
    .should('have.class', 'game__cell--userfilled')
})