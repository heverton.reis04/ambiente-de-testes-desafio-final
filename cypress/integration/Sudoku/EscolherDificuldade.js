Given('o inicio do Jogo',() => {
    cy.visit('https://sudoku-raravi.now.sh/')
})

When('selecionar uma dificuldade {string}', (dificuldade) => {
    cy.get('select[name="status__difficulty-select"]')
    .select(dificuldade)
})

Then('o tabuleiro muda',() => {
   // cy.screenshot()
})