Given('um Jogo em andamento',() => {
    cy.visit('https://sudoku-raravi.now.sh/')
})

And('definir a dificuldade como {string}',(dificuldade) => {
    cy.get('select[name="status__difficulty-select"]')
    .select(dificuldade)
})

When('iniciar um Novo Jogo',() => {
    cy.get('h2').contains('New Game').click()
})

Then('o tabuleiro muda',() => {

})

And('a dificuldade {string} permanece',(dificuldade) => {
    cy.get('select[name="status__difficulty-select"]')
    .should('contain',dificuldade)
})