Given('um Jogo em andamento',() => {
    cy.visit('https://sudoku-raravi.now.sh/')
})

And('que a dificuldade seja {string}',(dificuldade) => {
    cy.get('select[name="status__difficulty-select"]')
    .should('contain',dificuldade)
})

When('selecionar o Modo Anti-Erro',() => {
    cy.get('div[class="status__action-mistakes-mode"]')
    .click()
})

And('ao selecionar uma célula',() => {
    cy.wait(500)
    cy.get('tr.game__row:nth-child(1) > td:nth-child(1)')
    .click()
})

And('inserir o número {string}',(num) => {
    cy.get('div[class="status__number"]')
    .contains(num)
    .click()
})

Then('o campo deve estar vazio',() => {
    cy.wait(500)
    cy.get('tr.game__row:nth-child(1) > td:nth-child(1)')
    .should('contain', '0')
})