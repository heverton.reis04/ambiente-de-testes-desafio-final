And('tendo navegado para o estilo {string}',(estilo) => {
    cy.get('div[class="songs-header__genre "]')
    .contains(estilo)
    .click()
})

When('voltar a Home',() => {
    cy.get('div[class="nav__section"]')
    .contains('SoundRedux')
    .click()
})

Then('a página é carregada',() => {
    cy.wait(1500)
    cy.url().should('not.contain','?g=deep')
    cy.get('div[class="songs-header__genre songs-header__genre--active"]')
    .should('contain','house')
    cy.screenshot()
})