When('na página inicial',() => {
    cy.xpath('/html/body/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div[4]')
    .should('have.class','songs-header__genre--active')
})

And('o player não está visível',() => {
    cy.get('div[class="player"]')
    .should('not.exist')
})

Then('o site foi carregado',() => {
    cy.wait(1500)
    cy.screenshot()
})