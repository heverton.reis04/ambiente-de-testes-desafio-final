When('clicar no nome do artista',() => {
    cy.get('a[class="player__song__username"]')
    .click()
})

Then('a página do artista é exibida',() => {
    cy.wait(1500)
    cy.get('div[class="user-follow-button button button--short "]')
    .should('exist')
    cy.screenshot()
})