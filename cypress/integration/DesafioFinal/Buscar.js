When('buscar por {string}',(busca) => {
    cy.get('input[class="nav-search__input"]')
    .type(busca)
    .type('{enter}')
})

Then('o resultado é exibido',() => {
    cy.wait(1500)
    cy.screenshot()
})