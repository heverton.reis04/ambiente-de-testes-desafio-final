And('tocar algumas músicas',() => {
    for(var i = 1; i <= 3; i++){
        cy.get(`div.row:nth-child(1) > div:nth-child(${i})`)
        .click()
    }
})

When('acessar as recém tocadas',() => {
    cy.xpath('/html/body/div/div/div[3]/div/div/div[5]/div/div[3]')
    .click()
})

Then('a lista de tocadas é exibida',() => {
    cy.wait(1500)
    cy.get('div[class="history__header"]')
    .should('exist')
    .should('contain', 'Recently Played')
    cy.screenshot()
})