When('clicar no nome da música',() => {
    cy.get('a[class="player__song__title"]')
    .click()
})

Then('a página da música é exibida',() => {
    cy.wait(1500)
    cy.get('div[class="sidebar__header"]')
    .should('exist')
    .should('contain','Comments')
    cy.screenshot()
})