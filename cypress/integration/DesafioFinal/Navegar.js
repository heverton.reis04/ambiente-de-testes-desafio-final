When('navegar para {string}',(estilo) => {
    cy.get('div[class="songs-header__genre "]')
    .contains(estilo)
    .click()
})

Then('o estilo {string} é exibido',(estilo) => {
    cy.wait(1500)
    cy.get('div[class="songs-header__genre songs-header__genre--active"]')
    .should('contain', estilo)
    cy.screenshot()
})