When('filtar músicas lançadas a {string} dias',(data) => {
    cy.get('a[class="songs-header__time "]')
    .contains(`${data} days`)
    .click({force: true})
})

Then('as músicas dos últimos {string} dias são exibidas',(data) => {
    cy.wait(1500)
    cy.url()
    .should('contain', `?t=${data}`)
    cy.screenshot()
})