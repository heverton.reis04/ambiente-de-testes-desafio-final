When('manipular o volume',() => {
    cy.xpath('/html/body/div/div/div[3]/div/div/div[6]/div/div/div')
    .invoke('css','width','50%')
})

Then('o volume foi diminuido',() => {
    cy.wait(1500)
    cy.get('div[class="slider__bar__fill"]')
    .should('have.css','width','50px')
    cy.screenshot()
})