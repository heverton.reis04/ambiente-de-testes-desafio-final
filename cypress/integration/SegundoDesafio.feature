Feature: Operar Dashboard do Aplicativo ToDo

    COMO pessoa muito ocupada
    PRECISO operar a Dashboard de tarefas
    PARA que possa organizar minhas tarefas

    #Regra: todo item deve ter uma descrição
    #Regra: a lista deve estar vazia no primeiro acesso
    #Regra: a caixa de texto deve estar vazia ao adicionar itens
    Scenario: Adicionando um único item à Lista
        Given que não exista itens na Lista
        And a caixa de texto esteja vazia
        When tento adicionar o item "Estudar Testes"
        And pressiono a tecla Enter
        Then o item é adicionado
    #Regra: não é possivel adicionar items vazios
    Scenario: Adicionando um item vazio à Lista
        Given que a caixa de texto esteja vazia
        When tento adicionar o item " " a Lista
        Then o item não é adicionado
    #Regra: não é possivel adicionar descrições acima de 100 caracteres
    Scenario: Adicionando uma descrição muito longa à Lista
        Given que a caixa de texto esteja vazia
        When tento adicionar o item "kasjdaoksdfjokadsnaklsufdks aikodajskda sdopask dsaqjdklasud asdlaks djaskldu askdhçkask fdasmd lasd " a Lista
        And o item possui "101" caracteres
        Then o item não é adicionado
    #Regra: é possivel ter itens com mesma descrição
    Scenario: Adicionando itens com mesma descrição à Lista
        Given que exista o item "Estudar Testes" na Lista
        And a caixa de texto esteja vazia
        When tento adicionar novamente o item "Estudar Testes"
        And pressiono a tecla Enter
        Then o item é adicionado
    #Regra: é possível marcar um item como concluído ao clicar uma checkbox
    Scenario Outline: Marcando itens como concluído
        Given que exista "<item>" na Lista
        And "<item>" não esteja marcado como concluído
        When marco "<item>" como concluído
        Then "<item>" deve estar concluido
        Examples:
            | item                 |
            | Estudar Testes       |
    #Regra: é possível adicionar múltiplos itens à lista    
    Scenario Outline: Adicionando vários itens à Lista
        Given que a caixa de texto esteja vazia
        When tento adicionar "<item>" a Lista
        And pressiono a tecla Enter
        Then o item é adicionado
        Examples:
            | item                  |  
            | Lavar o carro         | 
            | Pintar a varanda      | 
            | Limpar a pia          | 
            | Dar banho no cachorro | 
            | Completar o Desafio   | 
            | Fazer um lanche       | 
            | Lavar a roupa         | 
            | Criar um Projeto Web  | 
            | Testar o Projeto Web  |
    #Regra: é possível excluir itens pendentes
    Scenario Outline: Excluindo itens pendentes
        Given que exista "<item>" na Lista
        And "<item>" não esteja marcado como concluído
        When tento excluir "<item>"
        Then "<item>" é excluido
        Examples:
            | item                 |
            | Limpar a pia         |
            | Criar um Projeto Web |
    #Regra: é possível excluir itens concluídos
    Scenario Outline: Excluindo itens concluídos
        Given que exista "<item>" na Lista
        And "<item>" esteja marcado como concluído
        When tento excluir "<item>"
        Then "<item>" é excluido
        Examples:
            | item            | 
            | Estudar Testes  |
            | Estudar Testes  |
    #Regra: é possivel ativar filtros no menu
    Scenario Outline: Ativando filtros
        Given que exista itens na lista
        When tento ativar "<filtro>"
        Then o filtro foi ativado
        Examples:
            | filtro       |
            | Filter       |
            | Progress     |
            | Show deleted |
    #Regra: é possível editar um item
    #Regra: não deve ser possível editar um item concluído
    #Regra: não deve ser possível editar um item excluído
    #Regra: é possível usar Enter para confirmar
    Scenario Outline: Editando um item da Lista
        Given que exista "<item>" na lista
        And "<item>" não tenha sido concluído
        And "<item>" não tenha sido excluido
        When edito "<item>" para "<novo item>"
        Then o item foi editado
        Examples:
            | item                | novo item        |
            | Pintar a varanda    | Pintar o Quarto  |
            | Completar o Desafio | Desafio Completo |
            | Lavar o carro       | Trocar o pneu    |
    #Regra: se houver muitos itens, quando scrollar até o final, 
    #       deve aparecer botão "Voltar ao Topo"
    #Scenario: Rolando a página
    #    Given que seja possivel rolar a página
    #    When vou até o final da página
    #    And se exite botão voltar ao Topo
    #    Then volto ao Topo