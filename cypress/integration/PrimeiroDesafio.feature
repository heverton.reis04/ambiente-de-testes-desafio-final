#language: pt
Funcionalidade: Organizar itens na lista TO DO
   COMO organizador de lista TO DO
   DESEJO organizar os itens na minha lista
   PARA que eu tenha controle sobre o que preciso fazer

   #Regra: Todo item tem uma descrição
   #Regra: É possível ter itens com a mesma descrição
   Esquema do Cenário: Novo item <resultado> à lista TO DO
      Dado que não exista nenhum item na lista TO DO
      Quando tento adicionar o item "<novo item>"
      Então o item "<resultado>"      
      Exemplos:
         | novo item           | resultado        | 
         | Estudar Testes      | é adicionado     |
         |                     | não é adicionado | 
         | Fazer Projeto Final | é adicionado     |
         | Estudar Testes      | é adicionado     |
         | Acordar as 03:00    | é adicionado     |
         | Fazer Exercícios    | é adicionado     |
   #Regra: É possível editar a descrição de itens da lista TO DO
   #Regra: É possível marcar como completo um item da lista
   #Regra: É possível excluir itens da lista TO DO
   Esquema do Cenário: Editar item da lista TO DO
      Dado que exista itens na lista TO DO
      Quando tento "<editar item>" adiciono um "<novo item>" no lugar
      Então o item "<editar item>" "<editado>" para "<novo item>"
      Exemplos:
         | editar item       | novo item            | editado              |
         | Estudar Testes    | Definir os Cenários  | foi alterado         |
         | Estudar Testes    |                      | não foi alterado     |
         | Acordar as 03:00  | Dormir cedo          | foi alterado         | 
         | Fazer Exercícios  | Caminhada na Orla    | foi alterado         |
         
   