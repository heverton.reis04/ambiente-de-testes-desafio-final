Given('que exista {string} na Lista', (item) =>{
    cy.visit('https://mastro-elfo.github.io/todo-react/#/dashboard')
    cy.get('span.MuiTypography-root').contains(item)
})

And('{string} esteja marcado como concluído',(item) => {
    cy.get('div[class="MuiButtonBase-root MuiListItem-root MuiListItem-gutters MuiListItem-button MuiListItem-secondaryAction"]')
    .contains(item)
    .then(() => {
        cy.get('input[type="checkbox"]')
        .should('be.checked')
    })
})

When('tento excluir {string}',(item) => {
    cy.get('div[class="MuiButtonBase-root MuiListItem-root MuiListItem-gutters MuiListItem-button MuiListItem-secondaryAction"]')
    .contains(item)
    .click()
    .get('button[class="MuiButtonBase-root MuiIconButton-root"]:visible')
    .click()
})

Then('{string} é excluido', (item) => {
    cy.get('ul[class="MuiList-root MuiList-padding"]')
    .should('not.contain', item)
    cy.screenshot({disableTimersAndAnimations: false})
})