Given('que a caixa de texto esteja vazia', () =>{
    cy.visit('https://mastro-elfo.github.io/todo-react/#/dashboard')
    cy.get('.MuiInputBase-input').should('be.empty')
})

When('tento adicionar o item {string}',(item) => {
    cy.get('.MuiInputBase-input').click().type(item)
})

And('o item possui {string} caracteres', (valor) => {
    cy.get('.MuiInputBase-input').invoke('val').should('have.length',valor)
})

Then('o item não é adicionado', () => {
    cy.get('.MuiInputAdornment-root > button:nth-child(1)').should('be.disabled')
    cy.screenshot({disableTimersAndAnimations: false})
})