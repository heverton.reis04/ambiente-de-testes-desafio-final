Given('que a caixa de texto esteja vazia', () =>{
    cy.visit('https://mastro-elfo.github.io/todo-react/#/dashboard')
    cy.get('.MuiInputBase-input').should('be.empty')
})

When('tento adicionar o item {string} a Lista',(item) => {
    cy.get('.MuiInputBase-input').click().type(item)
    cy.get('.MuiInputAdornment-root > button:nth-child(1)').should('be.disabled')
})

Then('o item não é adicionado', () => {
    cy.screenshot({disableTimersAndAnimations: false})
})