Given('que exista itens na lista', () =>{
    cy.visit('https://mastro-elfo.github.io/todo-react/#/dashboard')
    cy.get('ul[class="MuiList-root MuiList-padding"]').should('not.be.empty')
})

When('tento ativar {string}',(item) => {
    cy.get('button[class="MuiButtonBase-root MuiIconButton-root MuiIconButton-colorInherit"]')
    .click()
    .then(() => {
        cy.get('div[class="MuiPaper-root MuiDrawer-paper MuiDrawer-paperAnchorLeft MuiPaper-elevation16"]')
        .contains(item)
        .click()
    })
})

Then('o filtro foi ativado', () => {
    cy.screenshot({disableTimersAndAnimations: false})
})