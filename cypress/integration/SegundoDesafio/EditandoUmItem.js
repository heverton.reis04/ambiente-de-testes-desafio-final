Given('que exista {string} na lista', (item) =>{
    cy.visit('https://mastro-elfo.github.io/todo-react/#/dashboard')
    cy.get('span.MuiTypography-root').contains(item)
})

And('{string} não tenha sido concluído',(item) => {
    cy.get('div[class="MuiButtonBase-root MuiListItem-root MuiListItem-gutters MuiListItem-button MuiListItem-secondaryAction"]')
    .contains(item)
    .then(() => {
        cy.get('span[class="MuiButtonBase-root MuiIconButton-root jss655 MuiCheckbox-root MuiCheckbox-colorSecondary jss656 Mui-checked MuiIconButton-colorSecondary"]')
        .should('not.be.checked')
    })
})

And('{string} não tenha sido excluido',(item) => {
    cy.get('div[class="MuiButtonBase-root MuiListItem-root MuiListItem-gutters MuiListItem-button MuiListItem-secondaryAction"]')
    .contains(item)
    .then(() => {
        cy.get('div[class="MuiListItemSecondaryAction-root"]')
        .should('not.be.visible')
    })    
})

When('edito {string} para {string}',(item, novoItem) => {
    cy.get('span[class="MuiTypography-root MuiListItemText-primary MuiTypography-body1 MuiTypography-displayBlock"]')
    .contains(item)    
    .click()
    .then(() => {
        cy.get('input[class="MuiInputBase-input MuiInput-input"]')
        .click()
        .clear()
        .type(novoItem)
        .get('header[class="MuiPaper-root MuiAppBar-root MuiAppBar-positionFixed MuiAppBar-colorPrimary mui-fixed MuiPaper-elevation4"]')
        .click()
    })
    
})

Then('o item foi editado', () => {
    cy.screenshot({disableTimersAndAnimations: false})
})