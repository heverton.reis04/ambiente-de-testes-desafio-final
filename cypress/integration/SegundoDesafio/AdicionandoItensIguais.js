Given('que exista o item {string} na Lista', (item) =>{
    cy.visit('https://mastro-elfo.github.io/todo-react/#/dashboard')
    cy.get('span.MuiTypography-root').should('contain',item)
})

And('a caixa de texto esteja vazia',() => {
    cy.get('.MuiInputBase-input').should('be.empty')
})

When('tento adicionar novamente o item {string}',(item) => {
    cy.get('.MuiInputBase-input').click().type(item)
})

And('pressiono a tecla Enter', () => {
    cy.get('.MuiInputBase-input').type('{enter}')
})

Then('o item é adicionado', () => {
    cy.screenshot({disableTimersAndAnimations: false})
})