Given('que exista {string} na Lista', (item) =>{
    cy.visit('https://mastro-elfo.github.io/todo-react/#/dashboard')
    cy.get('span.MuiTypography-root').contains(item)
})

And('{string} não esteja marcado como concluído',(item) => {
    cy.get('div[class="MuiButtonBase-root MuiListItem-root MuiListItem-gutters MuiListItem-button MuiListItem-secondaryAction"]')
    .contains(item)
    .then(() => {
        cy.get('div[class="MuiListItemSecondaryAction-root jss423"]').should('not.be.checked')
    })
})

When('marco {string} como concluído',(item) => {
    cy.get('li[class="MuiListItem-container"]')
    .contains(item)
    .then(() => {
        cy.get('input[type="checkbox"]')
        .click({multiple:true})
    })
})

Then('{string} deve estar concluido', (item) => {
    cy.get('div[class="MuiButtonBase-root MuiListItem-root MuiListItem-gutters MuiListItem-button MuiListItem-secondaryAction"]')
    .contains(item)
    .then(() => {
        cy.get('input[type="checkbox"]')
        .should('be.checked')
    })
    cy.screenshot({disableTimersAndAnimations: false})
})