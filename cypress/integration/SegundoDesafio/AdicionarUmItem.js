Given('que não exista itens na Lista', () =>{
    cy.visit('https://mastro-elfo.github.io/todo-react/#/dashboard')
    cy.get('ul[class="MuiList-root MuiList-padding"]').should('be.empty')
})

And('a caixa de texto esteja vazia',() => {
    cy.get('.MuiInputBase-input').should('be.empty')
})

When('tento adicionar o item {string} a Lista',(item) => {
    cy.get('.MuiInputBase-input').click().type(item)
})

And('pressiono a tecla Enter', () => {
    cy.get('.MuiInputBase-input').type('{enter}')
})

Then('o item é adicionado', () => {
    cy.screenshot({disableTimersAndAnimations: false})
})