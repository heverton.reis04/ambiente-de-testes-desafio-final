const { afterEach } = require("mocha")

beforeEach(() => {
    cy.restoreLocalStorage();
})

Given('que a caixa de texto esteja vazia', () =>{
    cy.visit('https://mastro-elfo.github.io/todo-react/#/dashboard')
    cy.get('.MuiInputBase-input').should('be.empty')
})

When('tento adicionar {string} a Lista',(item) => {
    cy.get('.MuiInputBase-input').click().type(item)
})

And('pressiono a tecla Enter', () => {
    cy.get('.MuiInputBase-input').type('{enter}')
})

Then('o item é adicionado', () => {
    cy.screenshot({disableTimersAndAnimations: false})
})

afterEach(() => {
    cy.saveLocalStorage();
})