Given('o acesso ao site',() => {
    cy.visit('https://soundredux.io/#/')
    cy.get('#root').
    should('exist')
})

And('uma musica esteja tocando',() => {
    cy.xpath('/html/body/div/div/div[2]/div[2]/div/div[2]/div[1]/div[1]/div')
    .click()
    .then(() => {
        cy.get('div[class="player"]')
        .should('be.visible')
    })
})