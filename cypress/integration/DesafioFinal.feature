Feature: Navegar no SOUNDREDUX

    COMO analista de qualidade
    PRECISO navegar pelo SOUNDREDUX
    PARA avaliar a usabilidade

    @PrimeiroAcesso
    Scenario: Primeiro Acesso
        Given o acesso ao site
        When na página inicial
        And o player não está visível
        Then o site foi carregado
    #RN01: é possível buscar músicas ou artistas
    @Buscar
    Scenario Outline: Buscar Músicas e Artistas
        Given o acesso ao site
        When buscar por "<busca>"
        Then o resultado é exibido
        Examples:
            | busca                   |
            | Michael Jackson         |
            | The House Of Rising Sun |
    #RN02: é possível logar em um serviço externo
    @Logar
    Scenario: Logar no SoundCloud
        Given o acesso ao site
        When acessar o perfil
        Then uma nova janela é aberta
    #RN03: é possível navegar entre vários estilos
    @Navegar
    Scenario Outline: Navegar entre Estilos
        Given o acesso ao site
        When navegar para "<estilo>"
        Then o estilo "<estilo>" é exibido
        Examples:
            | estilo      |
            | dubstep     |
            | progressive |
    #RN04: é possível retornar à página inicial a qualquer momento
    @GoHome
    Scenario: Voltar a Home
        Given o acesso ao site
        And tendo navegado para o estilo "deep"
        When voltar a Home
        Then a página é carregada
    #RN05: é possível tocar qualquer música
    @Tocar
    Scenario: Tocar Música
        Given o acesso ao site
        When por uma música para tocar
        Then o player é exibido
    #RN06: é possíve acessar a página de uma música
    @GoMusic
    Scenario: Acessar Música
        Given o acesso ao site
        And uma musica esteja tocando
        When clicar no nome da música
        Then a página da música é exibida
    #RN07: é possível acessar a página de um artista
    @GoArtist
    Scenario: Acessar Artista
        Given o acesso ao site
        And uma musica esteja tocando
        When clicar no nome do artista
        Then a página do artista é exibida 
    #RN08: é possível acessar o historico das musicas tocadas
    @Historico
    Scenario: Histórico de Músicas
        Given o acesso ao site
        And tocar algumas músicas
        When acessar as recém tocadas
        Then a lista de tocadas é exibida
    #RN09: é possível alterar o volume da música
    @Volume
    Scenario: Alterar o Volume
        Given o acesso ao site
        And uma musica esteja tocando
        When manipular o volume
        Then o volume foi diminuido
    #RN10: é possível filtrar músicas pela data de lançamento
    @Filtrar
    Scenario Outline: Filtrar por Data
        Given o acesso ao site
        When filtar músicas lançadas a "<data>" dias
        Then as músicas dos últimos "<data>" dias são exibidas
        Examples:
            | data |
            | 7    |
            | 30   |
            | 90   |
