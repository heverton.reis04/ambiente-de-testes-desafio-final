Feature: Jogar Sudoku

    COMO Analista de Qualidade
    DESEJO testar o Game Sudoku
    PARA assegurar sua jogabilidade

    #Regra: o timer precisa iniciar zerado ao primeiro acesso
    Scenario: Primeiro Acesso
        Given o primeiro acesso ao Jogo
        And que a dificuldade seja "Easy"
        When o timer começar a correr
        Then é possível começar a Jogar
    #Regra: é possível escolher entre tres dificuldades
    Scenario Outline: Escolher Dificuldade
        Given o inicio do Jogo
        When selecionar uma dificuldade "<dificuldade>"
        Then o tabuleiro muda
        Examples:
            | dificuldade |
            | Easy        |
            | Medium      |
            | Hard        |
    #Regra: é possível reiniciar o jogo a qualquer momento
    Scenario Outline: Novo Jogo
        Given um Jogo em andamento
        And definir a dificuldade como "<dificuldade>"
        When iniciar um Novo Jogo
        Then o tabuleiro muda
        And a dificuldade "<dificuldade>" permanece 
        Examples:
            | dificuldade |
            | Easy        |
            | Medium      |
            | Hard        |
    #Regra: para usar o teclado é preciso clicar em uma célula
    @InserirNumero
    Scenario: Inserir número
        Given um Jogo em andamento
        And que a dificuldade seja "Easy"
        When selecionar uma célula
        And inserir o número "1"
        Then o campo recebeu o número "1"
    #Regra: só é possível desfazer ações quando um acélula tiver
    #       preenchida
    Scenario: Desfazer
        Given um Jogo em andamento
        And que a dificuldade seja "Easy"
        And ao selecionar uma célula
        And inserir o número "1"
        When desfizer a Ação
        Then o campo foi apagado
    #Regra: só é possível apagar uma célula editável
    Scenario: Apagar
        Given um Jogo em andamento
        And que a dificuldade seja "Easy"
        And ao selecionar uma célula
        And inserir o número "1"
        When apagar uma célula
        Then o campo foi apagado
    #Regra: é possível pedir dicas
    Scenario: Pedir Dica
        Given um Jogo em andamento
        And que a dificuldade seja "Easy"
        And ao selecionar uma célula
        When pedir uma Dica
        Then o campo é preenchido
    #Regra: o modo rápido permite escolher o número antes da célula
    Scenario: Modo Rápido
        Given um Jogo em andamento
        And que a dificuldade seja "Easy"
        When selecionar o Modo Rápido
        And inserir o número "1"
        And ao selecionar uma célula
        Then o campo recebeu o número "1"
    #Regra: se o modo antierro estiver ativado, não deve ser 
    #       permitido colocar um número errado
    Scenario: Modo Anti-Erro
        Given um Jogo em andamento
        And que a dificuldade seja "Easy"
        When selecionar o Modo Anti-Erro
        And ao selecionar uma célula
        And inserir o número "1"
        Then o campo deve estar vazio
    #Regra: ao ganhar o jogo, aparece uma mensagem
    
    #Regra: deve ser possível usar o teclado
