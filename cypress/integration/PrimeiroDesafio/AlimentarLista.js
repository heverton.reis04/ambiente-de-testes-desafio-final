beforeEach(() => {
    cy.restoreLocalStorageCache()
})

Given ('que não exista nenhum item na lista TO DO', () => { 
   cy.visit('https://just-another-todo-app.now.sh/')
   cy.get('[id=new-todo-item]').click()
})
When ('tento adicionar o item {string}', (item) => {
    if(item == '') {
        return
    }
    cy.get('[id=new-todo-item]').type(item)
    cy.get('button[data-testid="form-submit"]').click()
})
Then('o item {string}', (resultado) => {   
    //cy.screenshot({disableTimersAndAnimations: false})
})

afterEach(() => {
    cy.saveLocalStorageCache()
})

