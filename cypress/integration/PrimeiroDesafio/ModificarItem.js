beforeEach(() => {
    cy.restoreLocalStorageCache()
})

Given ('que exista itens na lista TO DO', () => { 
   cy.visit('https://just-another-todo-app.now.sh/')
   cy.get('[data-testid="form-item-item-completion"]').should('exist')
})
When ('tento {string} adiciono um {string} no lugar', (editarItem, novoItem) => {
    cy.screenshot({disableTimersAndAnimations: false})
    if( cy.get('.form_item__text').contains(editarItem) ){
        
        if(novoItem == ''){
            return
        } else {
            cy.get('[data-testid="form-item-item-completion"]').contains(editarItem).trigger('mouseover')
            cy.get('[data-testid="property-bar-edit-button"]').click()
            cy.get('[id="input-edit-todo-item"]').clear().type(novoItem)
            cy.get('[data-testid="form-edit-submit-button"]').click() 
        }   
    }
})
Then('o item {string} {string} para {string}', (editarItem, resultado, novoItem) => {
    if(resultado == 'foi alterado'){
        cy.get('[data-testid="form-item-item-completion"]').contains(novoItem).click()
        cy.screenshot({disableTimersAndAnimations: false})
    } else {
        cy.get('[data-testid="form-item-item-completion"]').contains(editarItem).trigger('mouseover')
        cy.get('[data-testid="property-bar-delete-button"]').click()
    }  
})

afterEach(() => {
    cy.saveLocalStorageCache()
})

